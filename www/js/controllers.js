angular.module('starter.controllers', [])


  .service('filter_service', function() {

    var deal_details = "";
    var level_inc = "";



    return {

      set_news_to_filter: function(val) {
        deal_details = val;
      },
      get_news_to_filter: function() {
        return deal_details;
      },
      set_newes_source: function(val) {
        level_inc = val;
      },
      get_newes_source: function() {
        return level_inc;
      }

    }


  })

  .controller('splashCtrl', function($ionicModal, $scope, $cordovaDevice, $cordovaAppVersion,
    $rootScope, $q, $http, $state, $interval, $ionicPlatform, $timeout, $cordovaDialogs, $window) {

    var currentMobile = window.localStorage.getItem('mobile');

    console.log('in splashCtrl');


    $timeout(function() {

      var hideMe = document.getElementById("custom-overlay");
      hideMe.style.display = "none";

      //document.getElementById("custom-overlay").style.display = "none";
    }, 7000);
    $scope.$on('some', function() {

      console.log('on change')
      $scope.$on('$ionicView.afterEnter', function() {
        console.log('ionic view after enter')
        $timeout(function() {

          var hideMe = document.getElementById("custom-overlay");
          hideMe.style.display = "none";
          console.log('ionic view after enter1')
          console.log('ionic view after enter2')
          'use strict';
          //  document.addEventListener("deviceready", onDeviceReady, false);
          console.log('ionic view after enter3')


          //onDeviceReady();

          //document.getElementById("custom-overlay").style.display = "none";
        }, 7000);

        console.log('ionic view after enter2')
        'use strict';
        //document.addEventListener("deviceready", onDeviceReady, false);
        console.log('ionic view after enter3')

        console.log('device ready');
        function onDeviceReady() {
               
        //console.log(window.Connection);
        if (window.Connection) {
          console.log('1');
          /* console.log(navigator.connection.type == Connection.NONE);*/
          if (navigator.connection.type == Connection.NONE) {
            console.log('2');
            swal(
              '',
              'No Network Connection!',
              'error'
            )
          } else {



            console.log('else')

            //$state.go('app.playlists');
            $state.go('intro')


          }


        }
         }//end of window connection if






      });


    });
    $scope.$emit('some');
  })

  .controller('IntroCtrl', function($scope, $cordovaGeolocation, $ionicPlatform, $state, $cordovaDialogs, $ionicSlideBoxDelegate) {

    $scope.options = {
      loop: true
    }

    console.log($ionicSlideBoxDelegate);
    // Called to navigate to the main app

    $scope.startApp = function() {
      //alert($scope.lat);
      //alert($scope.long);

      $state.go('app.playlists');
      //$state.go('register');
    };

    $scope.next = function() {

      $ionicSlideBoxDelegate.next();
    };
    $scope.previous = function() {

      $ionicSlideBoxDelegate.previous();
    };

    // Called each time the slide changes
    $scope.slideChanged = function(index) {

      $scope.slideIndex = index;
    };
  })
  .controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  
    $scope.loginData = {};

    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/login.html', {
      scope: $scope
    }).then(function(modal) {
      $scope.modal = modal;
    });

    // Triggered in the login modal to close it
    $scope.closeLogin = function() {
      $scope.modal.hide();
    };

    // Open the login modal
    $scope.login = function() {
      $scope.modal.show();
    };

    // Perform the login action when the user submits the login form
    $scope.doLogin = function() {
      console.log('Doing login', $scope.loginData);

      $timeout(function() {
        $scope.closeLogin();
      }, 1000);
    };
  })

  .controller('browseansctrl', function($ionicModal, $ionicHistory, $ionicPopup, $scope, $rootScope, $ionicPopup, $cordovaGeolocation,
    $ionicPlatform, $timeout, $ionicLoading, $window, $state, $http, $ionicPlatform,
    $ionicLoading, $location, $ionicHistory, $q, $cordovaDialogs, $ionicScrollDelegate, $ionicViewService, $stateParams, $ionicSideMenuDelegate, filter_service) {
    $rootScope.$ionicGoBack = function() {

      console.log("fired")
      $ionicHistory.goBack(-1);
      // $state.go('tab.chats')


    };

    $scope.browse_data = filter_service.get_news_to_filter();
    console.log($scope.browse_data)
    console.log($scope.browse_data.urlToImage);

    console.log(moment().format("MMM Do YY"));
    $scope.published_date = moment($scope.browse_data.publishedAt).format("MMM Do YY");





  })

  .controller('view_browseCtrl', function($ionicModal, $ionicHistory, $ionicPopup, $scope, $rootScope, $ionicPopup, $cordovaGeolocation,
    $ionicPlatform, $timeout, $ionicLoading, $window, $state, $http, $ionicPlatform,
    $ionicLoading, $location, $ionicHistory, $q, $cordovaDialogs, $ionicScrollDelegate, $ionicViewService, $stateParams, $ionicSideMenuDelegate, filter_service) {


    $rootScope.$ionicGoBack = function() {

      console.log("fired")
      $ionicHistory.goBack(-1);

    };

    $scope.browse_data = filter_service.get_news_to_filter();
    console.log($scope.browse_data)
    console.log($scope.browse_data.urlToImage);

    console.log(moment().format("MMM Do YY"));
    $scope.published_date = moment($scope.browse_data.publishedAt).format("MMM Do YY");





  })


  .controller('PlaylistsCtrl', function($ionicModal, $ionicHistory, $ionicPopup, $scope, $rootScope, $ionicPopup, $cordovaGeolocation,
    $ionicPlatform, $timeout, $ionicLoading, $window, $state, $http, $ionicPlatform,
    $ionicLoading, $location, $ionicHistory, $q, $cordovaDialogs, $ionicScrollDelegate, $ionicViewService, $stateParams, $ionicSideMenuDelegate, filter_service) {

    $scope.get_the_obj = function(val) {
      console.log(val);
      filter_service.set_news_to_filter(val);
      $state.go('app.browse');

    }

    $scope.check_show = function(val1, val2) {

      if (val2 == null) {

        return false;
      } else {
        return true;
      }
    }
    $scope.gotomenu = function() {

      // $state.go('app');
      $ionicSideMenuDelegate.toggleLeft();
      console.log($ionicSideMenuDelegate)

    }

    swal({
      text: 'Please Wait...',
      allowOutsideClick: false,
      timer: 50000,
      width: 500,
      hight: 100,
      onOpen: function() {
        swal.showLoading()
      }
    }).then(
      function() {},
      // handling the promise rejection
      function(dismiss) {
        if (dismiss === 'timer') {

        } else {
          swal('Please Wait...');
          swal.showLoading()
        }
      }
    )
    swal.showLoading()
    $http.get("https://newsapi.org/v2/top-headlines?country=us&apiKey=7964ddb85bc8447ea3239f7134298d0a", {

      })
      .success(function(data, status, headers, config) {

        console.log(data)

        $scope.top_headlines = data.articles;
        swal.closeModal();



      })
      .error(function(data, status, header, config) {

        // console.log($scope.data.error.status); 
        $ionicLoading.hide();
        swal.closeModal();
        //   $cordovaDialogs.alert('Ahhh! a technical glitch. Please try in sometime', 'Alert', 'OK');

        // ionic.Platform.exitApp();

        if (status == '503' || status == '500' || status == '501') {
          $cordovaDialogs.alert('Ahhh! a technical glitch. Please try in sometime', 'Alert', 'OK');
          ionic.Platform.exitApp();
        } else if (status == '0' || status == '408') {
          $cordovaDialogs.alert('Seems like slow internet connection, try in sometime', 'Alert', 'OK');
          ionic.Platform.exitApp();
        } else if (status == '404' || status == '400') {
          $cordovaDialogs.alert('Wrong turn, try in sometime', 'Alert', 'OK');
          ionic.Platform.exitApp();
        } else if (status == '401') {
          var id = 1;
          jwtservice.getToken(id).then(function(result) {
            console.log("got from factory")
            // $scope.level_inc  =  $scope.level_inc - 1;
            $scope.hi();
          });
        } else {
          $cordovaDialogs.alert('Ahhh! a technical glitch. Please try in sometime', 'Alert', 'OK');
          ionic.Platform.exitApp();
        }

      });
  })

  .controller('viewCtrl', function($ionicModal, $ionicHistory, $ionicPopup, $scope, $rootScope, $ionicPopup, $cordovaGeolocation,
    $ionicPlatform, $timeout, $ionicLoading, $window, $state, $http, $ionicPlatform,
    $ionicLoading, $location, $ionicHistory, $q, $cordovaDialogs, $ionicScrollDelegate, $ionicViewService, $stateParams, $ionicSideMenuDelegate, filter_service) {

    $scope.get_the_obj = function(val) {

      console.log(val);
      filter_service.set_news_to_filter(val);
      $state.go('app.view_browse');

    }

    $scope.check_show = function(val1, val2) {

      if (val2 == null) {

        return false;
      } else {
        return true;
      }
    }
    $scope.gotomenu = function() {

      // $state.go('app');
      $ionicSideMenuDelegate.toggleLeft();
      console.log($ionicSideMenuDelegate)

    }

    swal({
      text: 'Please Wait...',
      allowOutsideClick: false,
      timer: 50000,
      width: 500,
      hight: 100,
      onOpen: function() {
        swal.showLoading()
      }
    }).then(
      function() {},
      // handling the promise rejection
      function(dismiss) {
        if (dismiss === 'timer') {

        } else {
          swal('Please Wait...');
          swal.showLoading()
        }
      }
    )
    swal.showLoading()

    $scope.whole_data = filter_service.get_newes_source();
    //https://newsapi.org/v2/top-headlines?sources=bbc-news&apiKey=7964ddb85bc8447ea3239f7134298d0a
    $http.get("https://newsapi.org/v2/top-headlines?sources=" + $scope.whole_data.id + "&apiKey=7964ddb85bc8447ea3239f7134298d0a", {

      })
      .success(function(data, status, headers, config) {

        console.log(data)

        $scope.top_headlines = data.articles;
        swal.closeModal();



      })
      .error(function(data, status, header, config) {

        // console.log($scope.data.error.status); 
        $ionicLoading.hide();
        swal.closeModal();
        //   $cordovaDialogs.alert('Ahhh! a technical glitch. Please try in sometime', 'Alert', 'OK');

        // ionic.Platform.exitApp();

        if (status == '503' || status == '500' || status == '501') {
          $cordovaDialogs.alert('Ahhh! a technical glitch. Please try in sometime', 'Alert', 'OK');
          ionic.Platform.exitApp();
        } else if (status == '0' || status == '408') {
          $cordovaDialogs.alert('Seems like slow internet connection, try in sometime', 'Alert', 'OK');
          ionic.Platform.exitApp();
        } else if (status == '404' || status == '400') {
          $cordovaDialogs.alert('Wrong turn, try in sometime', 'Alert', 'OK');
          ionic.Platform.exitApp();
        } else if (status == '401') {
          var id = 1;
          jwtservice.getToken(id).then(function(result) {
            console.log("got from factory")
            // $scope.level_inc  =  $scope.level_inc - 1;
            $scope.hi();
          });
        } else {
          $cordovaDialogs.alert('Ahhh! a technical glitch. Please try in sometime', 'Alert', 'OK');
          ionic.Platform.exitApp();
        }

      });
  })


  .controller('SearchCtrl', function($ionicModal, $ionicHistory, $ionicPopup, $scope, $rootScope, $ionicPopup, $cordovaGeolocation,
    $ionicPlatform, $timeout, $ionicLoading, $window, $state, $http, $ionicPlatform,
    $ionicLoading, $location, $ionicHistory, $q, $cordovaDialogs, $ionicScrollDelegate, $ionicViewService, $stateParams, $ionicSideMenuDelegate, filter_service) {

    //https://newsapi.org/v2/sources?language=en&apiKey=7964ddb85bc8447ea3239f7134298d0a
    $scope.go_to_view = function(id) {

      filter_service.set_newes_source(id);
      $state.go('app.view');
    }
    $scope.gotomenu = function() {

      // $state.go('app');
      $ionicSideMenuDelegate.toggleLeft();
      console.log($ionicSideMenuDelegate)

    }
    swal({
      text: 'Please Wait...',
      allowOutsideClick: false,
      timer: 50000,
      width: 500,
      hight: 100,
      onOpen: function() {
        swal.showLoading()
      }
    }).then(
      function() {},
      // handling the promise rejection
      function(dismiss) {
        if (dismiss === 'timer') {

        } else {
          swal('Please Wait...');
          swal.showLoading()
        }
      }
    )
    swal.showLoading()
    $http.get("https://newsapi.org/v2/sources?language=en&apiKey=7964ddb85bc8447ea3239f7134298d0a", {

      })
      .success(function(data, status, headers, config) {

        console.log(data)

        $scope.top_sources = data.sources;
        swal.closeModal();



      })
      .error(function(data, status, header, config) {

        // console.log($scope.data.error.status); 
        $ionicLoading.hide();
        swal.closeModal();
        //   $cordovaDialogs.alert('Ahhh! a technical glitch. Please try in sometime', 'Alert', 'OK');

        // ionic.Platform.exitApp();

        if (status == '503' || status == '500' || status == '501') {
          $cordovaDialogs.alert('Ahhh! a technical glitch. Please try in sometime', 'Alert', 'OK');
          ionic.Platform.exitApp();
        } else if (status == '0' || status == '408') {
          $cordovaDialogs.alert('Seems like slow internet connection, try in sometime', 'Alert', 'OK');
          ionic.Platform.exitApp();
        } else if (status == '404' || status == '400') {
          $cordovaDialogs.alert('Wrong turn, try in sometime', 'Alert', 'OK');
          ionic.Platform.exitApp();
        } else if (status == '401') {
          var id = 1;
          jwtservice.getToken(id).then(function(result) {
            console.log("got from factory")
            // $scope.level_inc  =  $scope.level_inc - 1;
            $scope.hi();
          });
        } else {
          $cordovaDialogs.alert('Ahhh! a technical glitch. Please try in sometime', 'Alert', 'OK');
          ionic.Platform.exitApp();
        }

      });

  });
