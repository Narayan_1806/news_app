
angular.module('starter', ['ionic', 'starter.controllers','ngCordova'])

.run(function($ionicPlatform, $ionicPopup) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)

     if(window.Connection) {
            if(navigator.connection.type == Connection.NONE) {
                $ionicPopup.confirm({
                    title: "Internet Disconnected on your device",
                    content: "App requires Network Connection..."
                })
                .then(function(result) {
                    if(!result) {
                        ionic.Platform.exitApp();
                    }
                });
            }
        }


    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

    .state('splash', {
      cache:false,
        url: '/splash',
        templateUrl: 'templates/splash.html',
        controller: 'splashCtrl',

      })

     .state('intro', {
      cache:false,
        url: '/intro',
        templateUrl: 'templates/flashintro.html',
        controller: 'IntroCtrl'
      })

  .state('app.search', {
    cache:false,
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html',
        controller:'SearchCtrl'
      }
    }
  })

  .state('app.view', {
      cache:false,
      url: '/view',
      views: {
        'menuContent': {
          templateUrl: 'templates/view.html',
          controller: 'viewCtrl',
          reload: true
        }
      }
    })

  .state('app.browse', {
      url: '/browse',
      cache:false,
      views: {
        'menuContent': {
          templateUrl: 'templates/browse.html',
          controller:'browseansctrl',
          reload: true
        }
      }
    })

   .state('app.view_browse', {
      url: '/view_browse',
      cache:false,
      views: {
        'menuContent': {
          templateUrl: 'templates/view_browse.html',
          controller:'view_browseCtrl',
          reload: true
        }
      }
    })
    .state('app.playlists', {
      cache:false,
      url: '/playlists',
      views: {
        'menuContent': {
          templateUrl: 'templates/playlists.html',
          controller: 'PlaylistsCtrl',
          reload: true
        }
      }
    })

  .state('app.single', {
    url: '/playlists/:playlistId',
    views: {
      'menuContent': {
        templateUrl: 'templates/playlist.html',
        controller: 'PlaylistCtrl',
        reload: true
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/splash');
});
